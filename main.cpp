#include <iostream>
#include <thread>
#include <mutex>
#include <unistd.h>
#include <Display.h>
#include <condition_variable>

#define NUM_THREADS 60

using namespace std;

//Philosopher philosophers[NUM_THREADS];
mutex Forks[NUM_THREADS];
mutex block;
mutex lockix;
condition_variable CV;
bool ready = false;
bool getExit = false;
bool philosophizeOrEat;

void Philosophize(int executionTime, int place)
{
    lockix.lock();
    mvprintw(place,55,"            ");
    lockix.unlock();
    string s;
    lockix.lock();
    attron(COLOR_PAIR(3));
    mvprintw(place,55,"filozofuje");
    attroff(COLOR_PAIR(3));
    lockix.unlock();
    executionTime /= 20;
    for(int i=0; i<20; i++)
    {
        s = std::to_string(5*(i+1));
        char const *pchar = s.c_str();
        lockix.lock();
        mvprintw(place, 73+i, "#");
        mvprintw(place, 95, pchar);
        lockix.unlock();
        usleep(executionTime);
    }
    lockix.lock();
    Display::Clear(place);
    lockix.unlock();
}
//metoda jedz filozofa
void Eat(int executionTime, int place)
{
    lockix.lock();
    mvprintw(place,55,"            ");
    lockix.unlock();
    std::string s;
    lockix.lock();
    attron(COLOR_PAIR(2));
    mvprintw(place,55,"je");
    attroff(COLOR_PAIR(3));
    lockix.unlock();
    executionTime /= 20;
    for(int i=0; i<20; i++)
    {
        s = std::to_string(5*(i+1));
        char const *pchar = s.c_str();
        lockix.lock();
        mvprintw(place, 73+i, "#");
        mvprintw(place, 95, pchar);
        lockix.unlock();
        usleep(executionTime);
    }
    lockix.lock();
    Display::Clear(place);
    lockix.unlock();
}

//generator czasu z zakresu 2.5 - 3.5 sek
int GenerateTime()
{
    int randTime = 0;
    randTime = rand()%11 + 25;
    return randTime*100000;
}

int leftFork(int num)
{
    return num;
}

int rightFork(int num)
{
    return (num + 1) % NUM_THREADS;
}

void EatProcedure(int id)
{
  if (leftFork(id) < rightFork(id))
            {
                lockix.lock();
                mvprintw(id, 55, "Oczekuje");
                lockix.unlock();
                Forks[leftFork(id)].lock();
                lockix.lock();
                Display::PrintGreenFork(id, true);
                lockix.unlock();
                Forks[rightFork(id)].lock();
                lockix.lock();
                Display::PrintGreenFork(id, false);
                lockix.unlock();
                Eat(GenerateTime(), id);
                lockix.lock();
                Display::PrintRedForks(id);
                lockix.unlock();
                Forks[rightFork(id)].unlock();
                Forks[leftFork(id)].unlock();
            }
            else
            {
                            lockix.lock();
                mvprintw(id, 55, "Oczekuje");
                lockix.unlock();
                Forks[rightFork(id)].lock();
                lockix.lock();
                Display::PrintGreenFork(id, false);
                lockix.unlock();
                Forks[leftFork(id)].lock();
                lockix.lock();
                Display::PrintGreenFork(id, true);
                lockix.unlock();
                Eat(GenerateTime(), id);
                lockix.lock();
                Display::PrintRedForks(id);
                lockix.unlock();
                Forks[leftFork(id)].unlock();
                Forks[rightFork(id)].unlock();

            }
}

void layTheTable ()
{
    usleep(1000000);
    unique_lock<mutex> tlock(block);
    ready = true;
    mvprintw(5,0, "                                ");
    init_pair(4, COLOR_GREEN, COLOR_BLACK);
    attron(COLOR_PAIR(4));
    mvprintw(5,0, "Nakryto do stolu");
    for(int i = 0; i< NUM_THREADS; i++)
    {
        Display::PrintRedForks(i);
        mvprintw(i,68, "o");
    }
    CV.notify_all();
}

void print_thread_id (int id)
{
    unique_lock<mutex> tlock(block);
    while (!ready) CV.wait(tlock);
    tlock.unlock();
    philosophizeOrEat = rand()%2;
    if(philosophizeOrEat)
    {
        while (1)
        {
            if(getExit) break;
            Philosophize(GenerateTime(), id);
            if(getExit) break;
            EatProcedure(id);
        }
    }
    else
    {
        while (1)
        {
            if(getExit) break;
            EatProcedure(id);
            if (getExit) break;
            Philosophize(GenerateTime(), id);
        }
    }
}

int main()
{
    srand(time(NULL));
    thread threads[NUM_THREADS];
    initscr();
    timeout(1);
    curs_set(0);
    noecho();
    start_color();
    init_pair(1,COLOR_BLUE, COLOR_WHITE);
    init_pair(2,COLOR_RED, COLOR_BLACK);
    init_pair(3,COLOR_BLUE, COLOR_BLACK);
    init_pair(4, COLOR_GREEN, COLOR_BLACK);

    if( has_colors() == TRUE )
    {
        Display::PrintMenu(NUM_THREADS);
        for (int i=0; i<NUM_THREADS; ++i)
        {
            threads[i] = thread(print_thread_id, i);
        }
    }
    else printw( "Twoja konsole nie obsluguje kolorow" );
    thread go(layTheTable);

    for(;;)
    {
        char exit = getch();
        if(exit == 'q')
        {
            getExit = true;
            break;
        }
    }

    for(int i = 0; i < NUM_THREADS; i++)
    {
        threads[i].join();
    }
    go.join();
    endwin();

    return 0;
}

#ifndef DISPLAY_H
#define DISPLAY_H
#include <ncurses.h>
#include <string.h>
#include <string>

class Display
{
    public:
        Display();
        static void PrintRedForks(int);
        static void PrintGreenFork(int, bool);
        static void PrintMenu(int);
        static void Clear(int);
        virtual ~Display();

    protected:

    private:
};

#endif // DISPLAY_H

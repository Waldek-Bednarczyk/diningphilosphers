#include "Display.h"

using namespace std;
Display::Display()
{

}

void Display::PrintGreenFork(int id, bool leftOrRight)
{
    attron(COLOR_PAIR(4));
    if(leftOrRight) mvprintw(id,67, "Y");
    else mvprintw(id,69, "Y");
    attroff(COLOR_PAIR(4));
}

void Display::PrintRedForks(int id)
{
    attron(COLOR_PAIR(2));
    mvprintw(id,67, "Y");
    mvprintw(id,69, "Y");
    attroff(COLOR_PAIR(2));
}

void Display::PrintMenu(int NUM_THREADS)
{
    mvprintw(0,0, "Legenda:" );
    attron(COLOR_PAIR(3));
    mvprintw(1,0, "NIEBIESKI -");
    attroff(COLOR_PAIR(3));
    mvprintw(1,13, "filozofowanie");
    attron(COLOR_PAIR(2));
    mvprintw(2,0, "CZERWONY -");
    attroff(COLOR_PAIR(2));
    mvprintw(2,11, "jedzenie");
    attron(COLOR_PAIR(4));
    mvprintw(5,0, "Oczekiwanie na nakrycie do stolu");
    for (int i=0; i<NUM_THREADS; i++)
    {
        attron(COLOR_PAIR(1));
        string txt("Filozof numer ");
        txt += to_string(i);
        char text[100];
        strcpy(text,txt.c_str());
        mvprintw(i,35,text);
        attroff(COLOR_PAIR(1));
        mvprintw(i,72, "[");
        mvprintw(i,93, "]");
        mvprintw(i,98, "%%");
    }
    attroff(COLOR_PAIR(1));
}

void Display::Clear(int id)
{
    mvprintw(id,55,"          ");
    mvprintw(id,73,"                    ");
    mvprintw(id,95,"   ");
}

Display::~Display()
{
    //dtor
}
